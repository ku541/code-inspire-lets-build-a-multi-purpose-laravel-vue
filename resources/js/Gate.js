export default class Gate {
  /**
   * Create a gate.
   *
   * @param {string} authenticatedUserType
   * @param {Object} userTypes
   */
  constructor (authenticatedUserType, userTypes) {
    this.userTypes = userTypes
    this.authenticatedUserType = authenticatedUserType
  }

  /**
   * Check whether the authenticated user is an admin.
   *
   * @return {boolean}
   */
  allowsAdmin () {
    return this.authenticatedUserType === this.userTypes.admin
  }

  /**
   * Check whether the authenticated user is an admin or an author.
   *
   * @return {boolean}
   */
  allowsAdminOrAuthor () {
    return this.authenticatedUserType === this.userTypes.admin ||
           this.authenticatedUserType === this.userTypes.author
  }
}
