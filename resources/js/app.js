
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')

import moment from 'moment'
import Swal from 'sweetalert2'
import { Vue } from './bootstrap'
import EventBus from './EventBus'
import VueRouter from 'vue-router'
import VueProgressBar from 'vue-progressbar'
import NotFound from './components/NotFound.vue'
import { Form, HasError, AlertError } from 'vform'
import UserManager from './components/UserManager.vue'
import UserProfile from './components/UserProfile.vue'
import UserDashboard from './components/UserDashboard.vue'
import APIAuthentication from './components/APIAuthentication.vue'

Vue.use(VueRouter)
Vue.use(VueProgressBar, {
  color: '#28a745',
  failedColor: '#dc3545',
  height: '2px'
})

const routes = [
  { path: '/profile', component: UserProfile },
  { path: '/dashboard', component: UserDashboard },
  { path: '/manager', component: UserManager },
  { path: '/authentication', component: APIAuthentication },
  { path: '/404', component: NotFound },
  { path: '*', component: NotFound }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

Vue.component('pagination', require('laravel-vue-pagination'))

Vue.component(
  'not-found',
  require('./components/NotFound.vue').default
)

Vue.component(
  'example-component',
  require('./components/ExampleComponent.vue').default
)

Vue.component(
  'passport-clients',
  require('./components/passport/Clients.vue').default
)

Vue.component(
  'passport-authorized-clients',
  require('./components/passport/AuthorizedClients.vue').default
)

Vue.component(
  'passport-personal-access-tokens',
  require('./components/passport/PersonalAccessTokens.vue').default
)

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

new Vue({
  el: '#app',
  router,
  data: {
    query: ''
  },
  methods: {
    search: _.debounce(function () {
      EventBus.$emit('query-entered', this.query)
    }, 1000)
  }
})

Vue.filter('capitalize', function (value) {
  return typeof value === 'string'
    ? value.charAt().toUpperCase() + value.slice(1)
    : ''
})

Vue.filter('format', function (date) {
  return moment(date).format('MMMM Do YYYY')
})

export { Form, Toast }
