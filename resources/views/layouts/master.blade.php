@php
    $authenticatedUserType = 'user';
    $userTypes = config('user.types');

    if (auth()->check()) {
        $authenticatedUserType = auth()->user()->type;
    }
@endphp

@auth
    <script>
        window.authenticatedUserType = '@php echo $authenticatedUserType; @endphp'
        window.userTypes = @json($userTypes)
    </script>
@endauth

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Metis | Starter</title>

        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    </head>

    <body class="hold-transition sidebar-mini">
        <div id="app" class="wrapper">
            <!-- Navbar -->
            <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
                <!-- Left navbar links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#">
                            <i class="fa fa-bars"></i>
                        </a>
                    </li>
                </ul>

                <div class="input-group input-group-sm">
                    <input
                        @keyup="search"
                        v-model="query"
                        type="text"
                        class="form-control form-control-navbar"
                        placeholder="Search"
                        aria-label="Search"
                    >

                    <div class="input-group-append">
                        <button @click="search" class="btn btn-navbar" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </nav>
            <!-- /.navbar -->

            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4">
                <!-- Brand Logo -->
                <a href="index3.html" class="brand-link">
                    <img src="{{ asset('img/logo.png') }}" alt="Metis Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
                    <span class="brand-text font-weight-light">Metis</span>
                </a>

                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                        <div class="image">
                            <img src="{{ asset('img/user.jpg') }}" class="img-circle elevation-2" alt="User Image">
                        </div>

                        <div class="info">
                            <a href="#" class="d-block">{{ auth()->user()->name }}</a>
                            <span>{{ auth()->user()->type }}</span>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="nav-item">
                                <router-link to="/dashboard" class="nav-link">
                                    <i class="nav-icon fas fa-tachometer-alt blue"></i>
                                    <p>{{ __('Dashboard') }}</p>
                                </router-link>
                            </li>

                            <li class="nav-item has-treeview">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-cog green"></i>
                                    <p>
                                        {{ __('Management') }}
                                        <i class="right fa fa-angle-left"></i>
                                    </p>
                                </a>

                                @can('index-user')
                                    <ul class="nav nav-treeview">
                                        <li class="nav-item">
                                            <router-link to="/manager" class="nav-link">
                                                <i class="fas fa-users-cog yellow"></i>
                                                <p>User Manager</p>
                                            </router-link>
                                        </li>
                                    </ul>
                                @endcan
                            </li>

                            @can('view-authentication')
                                <li class="nav-item">
                                    <router-link to="/authentication" class="nav-link">
                                        <i class="nav-icon fas fa-code"></i>
                                        <p>{{ __('API Authentication') }}</p>
                                    </router-link>
                                </li>
                            @endcan

                            <li class="nav-item">
                                <router-link to="/profile" class="nav-link">
                                    <i class="nav-icon fas fa-user orange"></i>
                                    <p>{{ __('Profile') }}</p>
                                </router-link>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                >
                                    <i class="nav-icon fas fa-sign-out-alt red"></i>
                                    <p>{{ __('Logout') }}</p>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <router-view></router-view>

                        <vue-progress-bar></vue-progress-bar>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->

            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- To the right -->
                <div class="float-right d-none d-sm-inline">
                    Anything you want
                </div>

                <!-- Default to the left -->
                <strong>Copyright &copy; 2014-2018 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
            </footer>
        </div>
        <!-- ./wrapper -->

        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
