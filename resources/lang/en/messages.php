<?php

return [
    'error' => 'Failed to :action :item.',
    'success' => 'Successfully :action :item.'
];
