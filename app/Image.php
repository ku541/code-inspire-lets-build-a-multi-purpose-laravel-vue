<?php

namespace App;

use Exception;
use Intervention\Image\Facades\Image as Intervention;

class Image
{
    /**
     * Default avatar filename.
     */
    const DEFAULT_AVATAR = 'user.jpg';

    /**
     *The Intervention Image instance.
     */
    protected $intervention;

    /**
     * Create a new image instance.
     *
     * @param  Intervention  $intervention
     * @return void
     */
    public function __construct(Intervention $intervention)
    {
        $this->intervention = $intervention;
    }

    /**
     * Update the authenticated user's avatar in storage.
     *
     * @param  string  $currentAvatar
     * @return string
     */
    public function update($currentAvatar)
    {
        if (strpos(request('avatar'), 'data:image/') === false) {
            return $currentAvatar;
        }

        try {
            @unlink(public_path('img/profile/') . $currentAvatar);

            $filename = time() . '.jpeg';

            Intervention::make(request('avatar'))->save(
                public_path('img/profile/') . $filename
            );

            request()->merge(['avatar' => $filename]);

            return $filename;
        } catch (Exception $exception) {
            return self::DEFAULT_AVATAR;
        }
    }
}
