<?php

namespace App\Http\Controllers\API;

use App\Image;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image as Intervention;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the authenticated user's profile.
     *
     * @return \App\User
     */
    public function show()
    {
        return auth('api')->user();
    }

    /**
     * Update the authenticated user's profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Image $image)
    {
        $user = auth('api')->user();

        request()->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . $user->id,
            'password' => 'sometimes|required|min:8|max:16',
            'type' => 'required|string',
            'avatar' => 'nullable',
            'bio' => 'nullable|string'
        ]);

        $filename = $image->update($user->avatar);

        $updated = $user->update([
            'name' => request('name'),
            'email' => request('email'),
            'type' => request('type'),
            'avatar' => $filename,
            'bio' => request('bio')
        ]);

        if (request()->filled('password')) {
            $user->update(['password' => bcrypt(request('password'))]);
        }

        if (! $updated) {
            return response()->json(
                ['message' => __('messages.error', [
                    'action' => 'update',
                    'item' => 'profile'
                ])],
                Response::HTTP_NOT_FOUND
            );
        }

        return response()->json(
            [
                'message' => __('messages.success', [
                    'action' => 'updated',
                    'item' => 'profile'
                ]),
                'avatar' => $filename
            ],
            Response::HTTP_OK
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
