<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index()
    {
        $this->authorize('index-user');

        if ($query = request()->query('query')) {
            return User::whereName('like', "%$query%")
                       ->orWhere('email', 'like', "%$query%")
                       ->orWhere('type', 'like', "%$query%")
                       ->orWhere('bio', 'like', "%$query%")
                       ->paginate(10);
        }

        return User::latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return User
     */
    public function store()
    {
        request()->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|max:16',
            'type' => 'required|string',
            'avatar' => 'nullable',
            'bio' => 'nullable|string'
        ]);

        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'type' => request('type'),
            'avatar' => request('avatar'),
            'bio' => request('bio'),
        ]);

        if (! $user) {
            return response()->json(
                ['message' => __('messages.error', [
                    'action' => 'create',
                    'item' => 'user'
                ])],
                Response::HTTP_NOT_FOUND
            );
        }

        return response()->json(
            ['message' => __('messages.success', [
                'action' => 'created',
                'item' => 'user'
            ])],
            Response::HTTP_CREATED
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(User $user)
    {
        request()->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $user->id,
            'password' => 'sometimes|string|min:8|max:16',
            'type' => 'required|string',
            'avatar' => 'nullable',
            'bio' => 'nullable|string'
        ]);

        $updated = $user->update([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'type' => request('type'),
            'avatar' => request('avatar'),
            'bio' => request('bio')
        ]);

        if (! $updated) {
            return response()->json(
                ['message' => __('messages.error', [
                    'action' => 'update',
                    'item' => 'user'
                ])],
                Response::HTTP_NOT_FOUND
            );
        }

        return response()->json(
            ['message' => __('messages.success', [
                'action' => 'updated',
                'item' => 'user'
            ])],
            Response::HTTP_OK
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('delete-user');

        if (! $user->delete()) {
            return response()->json(
                ['message' => __('messages.error', [
                    'action' => 'delete',
                    'item' => 'user'
                ])],
                Response::HTTP_NOT_FOUND
            );
        }

        return response()->json(
            ['message' => __('messages.success', [
                'action' => 'deleted',
                'item' => 'user'
            ])],
            Response::HTTP_OK
        );
    }
}
