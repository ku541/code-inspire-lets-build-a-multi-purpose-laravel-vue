<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Gate::define('view-authentication', function ($user) {
            return $user->type === config('user.types.admin');
        });

        Gate::define('delete-user', function ($user) {
            return $user->type === config('user.types.admin');
        });

        Gate::define('index-user', function ($user) {
            return $user->type === config('user.types.admin') ||
                   $user->type === config('user.types.author');
        });
    }
}
